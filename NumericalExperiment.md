In accordance with this paper https://www.mdpi.com/2306-5729/5/2/43, each Numerical experiment should have a folder with a name consisting of the starting date of the experiment and \[optionally, but recommended\] some keyword explaining what the (nmerical) experiment is about: *YYYY*-*MM*-*DD*_*explanation*.
In the experiment folder, there should be a README.md containing some general information which will later help you remember what you did and link the experiment to the rest of your work. Of special significance is the yaml header at the readme, which contains machine processable information. The header has to be at the beginning of the document and it starts with the dashes ("---") followed by a new line and ends with a line containing only three dots(...). Thereafter there is free text for additionaly information.

As a reference, at the end of this document, there is an example.

Further tags can be looked up in the paper (https://www.mdpi.com/2306-5729/5/2/43). They can be used by caosdb. This is why it should be possible to machine read them.

## Example README.md file:


```
---
responsible: 
- Baltasar Rüchardt
description: Numerical experiment to create monodomain dynamics with Mitchell-Schaeffer local model, 
reconstruct the extracellular potential and calculate ecg signals in the Langendorff-perfusion bath. 
The structure of this folder is as follows. The Modules folder contains the self-written python 
Packages that were used. The Folder ExperimentalRealization contains the scripts to produce the data, 
each one in a subfolder, a folder with the Finite Element Meshes used and the solutions folder. 
The latter has a subfolder with the results for each initial condition. 
The Folder MeshCreation contains the files and scripts to create the Finite Element mesh.
results:
- file: ExperimentRealization/cluster-solutions/*_Create-Initial-Condition-ms/Results/02-dynamics-simulation/xdmf_u_n.{xdmf,h5}
  description: Dynamics data for visualisation in paraview
- file: ExperimentRealization/cluster-solutions/*_Create-Initial-Condition-ms/Results/02-dynamics-simulation/ts_u_n.h5
  description: Dynamics data to reuse in fenics / dolfin scripts with **same** mesh and functionspace.
- file:  ExperimentRealization/cluster-solutions/7*_Create-Initial-Condition-ms/Results/05-calculate-electrode-signals/electrode*.npy
  description: files containing the individual electrode signals
- file:  ExperimentRealization/cluster-solutions/7*_Create-Initial-Condition-ms/Results/02-on_regular_grid/voxel_coordinates.npy
  description: Containing the coordinates of a regular grid to which the FEM result was projected
- file:  ExperimentRealization/cluster-solutions/7*_Create-Initial-Condition-ms/Results/02-on_regular_grid/griddata.h5
  description: Contains the griddata of the regular grid over time.
- file:  ExperimentRealization/cluster-solutions/7*_Create-Initial-Condition-ms/Results/02-on_regular_grid/times.npy
  description: Contains the times of the griddata.
- file: \*\*/parameter.csv
  description: information about the parameters used in each of the single simulation steps
revisionOf: ../2021-02-18_VentricleInLangendorff
...
```

Note: I corrected for some errorneous ts_u_n.h5 files. This is why this is a revision

Info: How to read in the voxel data. One possible solution:

Open for example `ExperimentRealization/Solutions/*_Create-Initial-Condition-ms/Results/02-dynamics-simulation/xdmf_u_n.h5`.
This file will contain the coordinates of the voxel as: h5-path `"Mesh/0/mesh/geometry"` and the voxel values under
`"VisualisationVector/*step*"`, e.g. `"VisualisationVector/0"` for the first timestep.

Overall in python the data can be loaded via:

```python
import h5py
import numpy as np
path = "/path/to/xdmf_u_n.h5"
with h5py.File(path, "r") as hdf5_file:
    coords = np.empty(hdf5_file["Mesh/0/mesh/geometry"].shape)
    coords[:] = hdf5_file["Mesh/0/mesh/geometry"]
    values = np.empty(hdf5_file["VisualisationVector/0"].shape)
    num_timesteps = len(h5file["VisualisationVector"].keys())
    for timestep in range(num_timesteps):
      # retrieve values for timestep timestep
      values[:] = hdf5_file[f"Mesh/{timestep}/mesh/geometry"]
```

-----
