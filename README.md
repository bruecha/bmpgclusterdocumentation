[[_TOC_]]

Working with the computer cluster
===============

You have to understand a little bit on how computer work to use the cluster. There will be a as-simple-as-possible introduction at the beginning.

After that there will be a explanation on the cluster that is used at the MPI-DS with examples for the research group biomedical physics.

There is a course `Using the GWDG Scientific Compute Cluster` which provides some useful general information also for this cluster. See [here](https://www.gwdg.de/en/academy).


How computers work
===============

From a very simplified point of view a computer has two relevant parts: 1. The computer chip which performs tasks. 2. The hard drives which contain a) instructions how to perform a task (for example programs are just such instructions) and b) data that are necessary to perform a task.

We can now have a simplified understanding of what happens when we start a program. The chip reads the instructions and follows them one by one. It could for example be: Read the data stored on the harddrive and position xy, add them and write the data to the position xyz.

Whatever you see and do on the computer, it's the computer chip that follows some instructions that were defined before.

How you interact with the computer
===============

Without experience you interact probably via a graphical user interface (mouse, clicking on folders, etc.). Don't mind, this is completely fine! But to work with the cluster you will have to learn to work closer to the chip.

You can directly interact with it via *bash*. This is a shell in which you give commands directly to the chip. For example you can move in the data storage system, tell the chip to run a program, etc. [Basically everything.]. A starting point would be [this page](https://www.grymoire.com/Unix/Sh.html) or you take some days and read an introduction to bash.

For example you start bash and are in your home directory (A folder in the file system that is reserved for you). You can use `cd` to *c*hange *d*irectory to move inside the file system, `ls` to list the files in the folder you are in, `rm` to remove files and many more. Usually you find out how to do something by googleing what you want to do and entering bash, e.g. `copy files bash` will give you a hint h ow to copy files from the bash shell. `cd`, `ls` and `rm` are programs that have been written by others and are there by default. If you type a command in bash, the computer knows where to search for the instructions from something that is called PATH-variable. The PATH variable contains locations on the data system where the chip looks for the instructions. You can display it by typing `echo $PATH`. If you run a program that you think should be there but it isn't, you can know what might be the problem: It was not in the PATH, so the chip couldn't find it.




How does the cluster work?
===============


If you are working with the cluster, there are three computers to consider. This means: Three 'computers'. (Of course this is simplified but it is enough for now):

1. Your local computer: The computer / laptop you use. This is not related to the cluster but you need it to log into the cluster computers
2. The 'frontend'. This is a computer used to interact with the real cluster computers.
3. The 'backend'. This are the actual cluster computers that perform the instructions you give to them.



How you interact with the cluster
===============

You have to go to the frontend and tell the cluster administration program to put your program on the cluster.

The cluster administration program used in our institute is called sun grid engine.

There are basically two options to tell the cluster to calculated:

1. via the qsub command and a script that then runs on the backend (cluster computers)
  1. use `qsub [... options...] *your_script* script arguments`
  2. use a script in which you write the options with the `#$` syntax. (see [this](one-submit-script-example/Parallel_Header.sh) example script.)
2. via a script that runs on the frontnode and runs the qsub script to push thing to the backend computers.
  1. Advantages: 
    - You can run the submit backend script on the frontend to test it
    - You have the qsub command saved somewhere -> reproducibility is improved 
  2. Disadvantage: 
    - You have two scripts, you may have to program a bit in bash

Todo:
  - Interact with the cluster (submission)
  - qsub vs submission scripts
  - examples and what they do


The cluster data structure
===============

There are different ways to store data on the cluster. The following summary is based on information in the file [General Information](FurtherDocuments/GeneralInformation.pdf).

| Access via | use for | backuped | 
| ---- | ----- | ------ |
| /data.bmp/*username*/ | larger personal data, not final results, etc. | yes |
| /data.bmp/heart/ | final resuts | yes |
| /home/*username* | personal data (e.g. Emails, presentations) | yes |
| /scratch0X.local/*username* | data writing on the cluster @todo: Answer if I should copy data to scratchxx.local to perform calculations with them | no (even though protected against hard disc failure via the [RAID](https://en.wikipedia.org/wiki/RAID) system) |
| /data/*username* | ?? | ?? |

Most important is that you use for writing data the /scratch0x.local servers and copy or even better rsync data to your other directories afterwards.


Contact Email Adress
===============

Contact the IT team via hpc@ds.mpg.de


Proposed working process
===============

Python code
--------------

### Packages you write

I would recommend to run your code in python environments. I currently use anaconda to do this. Read about how to setup anaconda environments by googling it. Why is it recommened to use an environment? Like this you are independent on software installed at the cluster from the admins and your science is more likely to be reproducible.

Develop packages that you want to reuse in several scripts / numerical experiments in a folder in your home directory. E.g. `~/PhD/CodeDevelopment/packageName`. If you are not familiar with git, find a good git video tutorial, watch it and give the link to this manual. I recommend installing the packages. An example package is in the folder [examplePythonPackageRepository](examplePythonPackageRepository). You should choose a license and change the files [setup.cfg](examplePythonPackageRepository/setup.cfg) and the [README.md](examplePythonPackageRepository/README.md).

Once you have this configuration, you can install the package in your anaconda environment by
```bash
cd ~/PhD/CodeDevelopment/packageName
conda activate environment_name
pip install .
```
Thereafter you can use the package in the environment in every script, etc.

### Numerical experiments you perform & proposed data structure

The group wrote a [paper](https://www.mdpi.com/2306-5729/5/2/43) about a standardized file system layout and of course we recommend following it. Based on the paper, I recommend you adapt the following data structure to your needs:

At `/data.bmp/*username*` you create the following folders if you need them:
- `SimulationData`
- `DataAnalysis`

In those folders you create for each project an individual project folder with the pattern `YYYY_description`, e.g. `2021_complexity`. This folder groups all the numerical experiments / data analyses you perform in this project. Each analysis / numerical experiment has its own folder with the pattern `YYYY-MM-DD_description`, where `YYYY-MM-DD` is the approximate starting date (the exact day doesn't matter but it is incredibly helpful to have a feeling about when what was done). An example from my current work would be: `2021-04-15_VentricleInLangendorffCorrected`.

Inside the data analysis or the numerical experiment folder you can choose whatever you need. A folder for my current numerical experiments looks as follows:

[...]/2021-04-15_VentricleInLangendorffCorrected contains:
- scripts: scripts I wrote
- apply_scripts: frontend scripts to run the scripts in the 'scripts' folder on the backend
- routines: At one point I lost the overview and created an additional folder with script-like files
- ExperimentalRealization: This is where the scripts that perform the numerical simulation are in and it contains:
  - cluster_solutions: A link to `/scratch01.local/baltasar/YYYY_description/YYYY-MM-DD_description/`, e.g. `/scratch01.local/baltasar/2021_complexity/2021-04-15_VentricleInLangendorffCorrected`.
  - *other experiment related folders*, e.g. FEM-meshes
- README.md: A readme for the experiment.


Requirements to use the cluster
===============

Add `source /core/uge/LFPN/common/settings.sh` to your `~/.bashrc` on the frontend computer. This is necessary to load the sun grid engine related configurations.

Create a cluster pair key as documented in `/core/userconfig/HOWTO-use-SSH-keys-in-parallel-Qs` (frontend cluster, i.e. sirona0x). If you will use the parallel queue, write to Denny or Hecke -- they have to activate your account for the parallel queue.


Compile Software
===============

Software compiled by Hecke is under /usr/lfpn and software compiled by group members under /usr/bmp.


Useful stuff
===============

- Use the hdf5 file format: [Python introduction](https://docs.h5py.org/en/stable/quick.html), [Python Blogpost](https://www.pythonforthelab.com/blog/how-to-use-hdf5-files-in-python/)

- Useful aliases and functions on the frontend bashrc:
 
```bash
# shows summary on the queues indicated after -q
alias Qsummary='qstat -q "grannus.q,mvapich2-grannus.q" -g c'
# shows all jobs of all ussers
alias Qall="qstat -u '*'"
# shows jobs on indicated qs from *other* users
alias Qothers='qstat -q "grannus.q,mvapich2-grannus.q" -u "*" | grep -v $USER'
# gives a summary about the qs used
alias Qlistallqs="qstat -g c"
# print how many jobs of you are running 9on al qs)
alias QnumMyjobsRunning="qstat | grep ' r ' | wc -l"
# prints how much of your available space you use on the mounted file systems
alias DiskUsage="df -h | grep $USER"```
@todo add sshcd function
```

- Useful local computer aliases / functions **You have to add your MPI-ds username!**

```bash
scpToMpi(){
    # replace /local/home with /home/mpi-username/. This is necessary if ~ was expaned.
    local remote_file="${2/\/local\/home//home/mpi-username}"
    command scp -r -o 'ProxyJump mpi-username@10.218.100.51' "$1" mpi-username@sirona03:"$remote_file"
}


scpFromMpi(){
    # replace /local/home with /home/mpi-username/. This is necessary if ~ was expaned.
    local remote_file="${1/\/local\/home//home/mpi-username}"
    command scp -r -o 'ProxyJump mpi-username@10.218.100.51' mpi-username@sirona03:"$remote_file" "$2"
}

rsyncToCaosDB(){
    # replace /local/home with /home/caosdb-username/. This is necessary if ~ was expaned.
    local remote_file="${2/\/local\/home//home/caosdb-username}"
    command rsync -rltpPuzh --append "$1" caosdb-username@134.76.17.187:"$remote_file"
}
```

- if you work remote, there might be situations in which you need a shell on the cluster and then log out. This can be done with [screen](https://linuxize.com/post/how-to-use-linux-screen/).

