Provides a function to obtain a unique identifier for the simulation on the cluster.



Usage:

```python
from ClusterHelpers import get_ids

sim_id, job_id, task_id = get_ids()
```

This will provide the following information:

`job_id`: the number of the sge job (whenever you use qsub, you submit one *job*)
`task_id`: If you run a array job, this gives the number of the job in the array.
`sim_id`: A unique id for each task consisting of: `job_id`\_`task_id`\_`job_name`




