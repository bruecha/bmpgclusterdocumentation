"""
id functions
"""
import os

__all__ = ["get_ids", "on_cluster"]


def get_ids():
    """Return job_id, task_id and sim_id for cluster (=sun grid engine) jobs.
    Args:
        -
    Returns:
        - job_id: the job id
        - task_id: the id of the array 'sub job'
        - sim_id: for me: unique identifier for each simulation.
          It is given by job_id + '_' + task_id + '_' + job_name

    Notes:
        if the script is not used on the cluster, the following defaults will be used:
            - job_i: 'None'
            - job_name: 'no-sge-job'
            - task_id: 0
        if the script is run on the cluster but not as array job, task_id defaults to 0.

    """
    job_id = str(os.getenv("JOB_ID", "None"))
    job_name = str(os.getenv("JOB_NAME", "no-sge-job"))
    # task_id will be used in the script so make it something it can be.
    task_id = str(os.getenv("SGE_TASK_ID", "1"))
    # make it zero based
    try:
        task_id = int(task_id) - 1
    except ValueError:
        # probably run on sge but not as array job
        task_id = 0
    sim_id = job_id + "-" + str(task_id) + "_" + job_name
    return job_id, task_id, sim_id


def on_cluster():
    """Check if a cluster job is running by checking the environment variables.
    Returns True if on cluster, False else.

    Returns:
        - bool
    """
    # every clusterjob has a job_name, so this is enough for checking.
    if os.getenv("JOB_NAME") is None:
        return False
    else:
        return True
